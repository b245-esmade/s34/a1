

const express = require('express')
const port = 3000;
const app = express();

app.use(express.json());

// mock database
let users = [
		{
			username: "TStark300",
			email: "stark@gmail.com",
			password: "notPeterParker"
		},
		{
			username: "ThorThunder",
			email: "love@gmail.com",
			password: "iLoveStormBreaker"
		}
	]


// 1-2

		app.get("/home", (request,response) =>
		{
			response.status(201).send("Welcome to the homepage!")
		})


// 3-4
		app.get("/items", (request,response) =>
		{
			response.status(201).send(users)
		})


// 5-6
		app.delete("/delete-item", (request,response) =>
		{
			let deleteItem = users.pop()
			response.status(201).send(deleteItem)
		})



		app.listen(port,()=>console.log(`Server is running at ${port}!`))